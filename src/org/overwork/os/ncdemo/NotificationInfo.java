package org.overwork.os.ncdemo;

import android.view.accessibility.AccessibilityEvent;

/**
 * Notification 資料模包裝
 * @author Overing
 */
public class NotificationInfo {

    public String pkg;
    public int id;

    public static NotificationInfo parseByAccessibilityEvent(AccessibilityEvent evt) {
        // notification id 是拿不到的
        // 不過大多數的 App 開發者都會貫性用 0
        return new NotificationInfo(evt.getPackageName().toString(), 0);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + ((pkg == null) ? 0 : pkg.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        NotificationInfo other = (NotificationInfo) obj;
        if (id != other.id)
            return false;
        if (pkg == null) {
            if (other.pkg != null)
                return false;
        } else if (!pkg.equals(other.pkg))
            return false;
        return true;
    }

    private NotificationInfo(String pkg, int id) {
        this.pkg = pkg;
        this.id = id;
    }

}
