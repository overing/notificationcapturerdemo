package org.overwork.os.ncdemo;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.overwork.os.ncdemo.R;
import org.overwork.os.ncdemo.dirty.ReflectHack;
import org.overwork.os.ncdemo.service.NotificationCaptureService;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends ListActivity //
        implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String ACTION_UPDATE = "update",
            EXTRA_EVENT = "event";

    private BroadcastReceiver mBroadcastReceiver;
    private BaseAdapter mAdapter;
    private List<NotificationInfo> mNotifications;

    public static void updateBroadcast(Context cx, AccessibilityEvent evt) {
        cx.sendBroadcast(new Intent(ACTION_UPDATE).putExtra(EXTRA_EVENT, evt));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_add_notification) {
            NotificationCompat.Builder nb = new NotificationCompat.Builder(this);
            nb.setAutoCancel(true);
            nb.setSmallIcon(R.drawable.ic_launcher);
            nb.setContentTitle(getString(R.string.app_name));
            nb.setContentText("App self add notification for cancel test");
            nb.setDefaults(Notification.DEFAULT_SOUND);
            ((NotificationManager) getSystemService(NOTIFICATION_SERVICE))
                    .notify(0, nb.build());
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        final NotificationInfo info = mNotifications.get(position);
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Try to cancel the notification ?");
        adb.setMessage(String.format("Application: %s\nNotification ID: %d", info.pkg, info.id));
        adb.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        cancelNotification(info);
                    }
                });
        adb.setNegativeButton(android.R.string.cancel, null);
        adb.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: " + savedInstanceState);
        super.onCreate(savedInstanceState);

        mBroadcastReceiver = new UpdateBroadcastReceiver();
        mAdapter = new NotificationInfosAdapter();
        mNotifications = new ArrayList<NotificationInfo>();

        if (!NotificationCaptureService.isServiceEnable(this)) {
            AlertDialog.Builder adb = new AlertDialog.Builder(this);
            adb.setTitle("Need enable accessibility service");
            adb.setMessage(getString(R.string.app_name));
            adb.setPositiveButton("Go settings",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(
                                    Settings.ACTION_ACCESSIBILITY_SETTINGS));
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                    });
            adb.setNegativeButton(android.R.string.cancel,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                    });
            adb.setCancelable(false);
            adb.show();
        } else {
            setContentView(R.layout.activity_main);
            setListAdapter(mAdapter);
            getListView().setEmptyView(findViewById(android.R.id.empty));

            findViewById(R.id.btn_add_notification).setOnClickListener(this);
        }
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        registerReceiver(mBroadcastReceiver, new IntentFilter(ACTION_UPDATE));
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        unregisterReceiver(mBroadcastReceiver);
        super.onPause();
    }

    private void cancelNotification(NotificationInfo info) {
        try {
            ReflectHack.$.cancelNotificationWithTag(info.pkg, null, info.id);

            mAdapter.notifyDataSetInvalidated();
            mNotifications.remove(info);
            mAdapter.notifyDataSetChanged();
        } catch (Exception ex) {
            Log.e(TAG, "On cancel notification fail", ex);

            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            AlertDialog.Builder adb = new AlertDialog.Builder(this);
            adb.setTitle("On cancel notification fail");
            adb.setMessage(sw.toString());
            adb.setPositiveButton(android.R.string.cancel, null);
            adb.show();
        }
    }

    private class UpdateBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_UPDATE.equals(intent.getAction())) {
                AccessibilityEvent evt = intent.getParcelableExtra(EXTRA_EVENT);
                Notification notification = (Notification) evt
                        .getParcelableData();
                Log.w(TAG, "Notification: " + notification);

                NotificationInfo info = NotificationInfo
                        .parseByAccessibilityEvent(evt);
                if (!mNotifications.contains(info)) {
                    mAdapter.notifyDataSetInvalidated();
                    mNotifications.add(info);
                    mAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    private class NotificationInfosAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mNotifications.size();
        }

        @Override
        public NotificationInfo getItem(int position) {
            return mNotifications.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(MainActivity.this,
                        android.R.layout.simple_list_item_2, null);
            }

            NotificationInfo info = mNotifications.get(position);
            ((TextView) convertView.findViewById(android.R.id.text1)).setText("Application: " + info.pkg);
            ((TextView) convertView.findViewById(android.R.id.text2)).setText("Notification ID: " + info.id + "");

            return convertView;
        }
    }
}
