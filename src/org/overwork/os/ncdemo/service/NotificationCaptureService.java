package org.overwork.os.ncdemo.service;

import java.util.List;

import org.overwork.os.ncdemo.MainActivity;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.accessibilityservice.AccessibilityServiceInfoCompat;
import android.support.v4.view.accessibility.AccessibilityManagerCompat;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;

public class NotificationCaptureService extends AccessibilityService {

    private static final String TAG = NotificationCaptureService.class.getSimpleName();

    public static boolean isServiceEnable(Context cx) {
        AccessibilityManager am = (AccessibilityManager) cx.getSystemService(ACCESSIBILITY_SERVICE);
        int flag = AccessibilityServiceInfoCompat.FEEDBACK_ALL_MASK;
        List<AccessibilityServiceInfo> list = AccessibilityManagerCompat.getEnabledAccessibilityServiceList(am, flag);
        ComponentName cn = new ComponentName(cx, NotificationCaptureService.class);
        String mid = cn.flattenToShortString();
        for (AccessibilityServiceInfo s : list) {
            if (mid.equals(AccessibilityServiceInfoCompat.getId(s))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        Log.d(TAG, "onAccessibilityEvent");
        if (event.getEventType() == AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED) {
            MainActivity.updateBroadcast(this, event);
        }
    }

    @Override
    public void onInterrupt() {
        Log.d(TAG, "onInterrupt");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.eventTypes = AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED;
        info.feedbackType = AccessibilityServiceInfoCompat.FEEDBACK_ALL_MASK;
        info.notificationTimeout = 100;
        setServiceInfo(info);
        return super.onStartCommand(intent, flags, startId);
    }
}
