package org.overwork.os.ncdemo.dirty;

import android.app.NotificationManager;

/**
 * 包裝用反射機制從 NotificationManager 中<br>
 * 抽出來的 notification cancel method<p>
 * 已知問題: 一樣只能 cancel 由自己 App 發出的 notification<br>
 * 嘗試去 cancel 別的 App 的底層會丟出 SecurityException 說 UID 不相符<br>
 * 推測如果用 Root 權限執行 App 也許能得逞...
 * 
 * @author Overing
 */
public enum ReflectHack {
    $;

    public static final int UID_ALL = -1;

    public static Object getINotificationManager() throws Exception {
        return NotificationManager.class.getDeclaredMethod("getService").invoke(null);
    }

    /**
     * Checked Android Version<br>
     * 2.3(10), 4.0(14)
     */
    public void cancelNotification(String pkg, int nid) throws Exception {
        Object inm = getINotificationManager();
        Class<?>[] types = { String.class, int.class };
        inm.getClass().getDeclaredMethod("cancelNotification", types).invoke(inm, pkg, nid);
    }

    /**
     * Checked Android Version<br>
     * 2.3(10), 4.0(14)
     */
    public void cancelNotificationWithTag(String pkg, String tag, int nid) throws Exception {
        Object inm = getINotificationManager();
        Class<?>[] types = { String.class, String.class, int.class };
        inm.getClass().getDeclaredMethod("cancelNotificationWithTag", types).invoke(inm, pkg, tag, nid);
    }

    /**
     * Checked Android Version<br>
     * 4.2(17)
     */
    public void cancelNotificationWithTag(String pkg, String tag, int nid, int uid) throws Exception {
        Object inm = getINotificationManager();
        Class<?>[] types = { String.class, String.class, int.class, int.class };
        inm.getClass().getDeclaredMethod("cancelNotificationWithTag", types).invoke(inm, pkg, tag, nid, uid);
    }

}
